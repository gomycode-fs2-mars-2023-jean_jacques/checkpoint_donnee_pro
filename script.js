
const jeu1 = [3, 1, 7, 9];
const jeu2 = [2, 4, 1, 9, 3];
let somme = 0;

for (let i = 0; i < jeu1.length; i++) {
  if (!jeu2.includes(jeu1[i])) {
    somme += jeu1[i];
  }
}

for (let i = 0; i < jeu2.length; i++) {
  if (!jeu1.includes(jeu2[i])) {
    somme += jeu2[i];
  }
}

console.log(somme);